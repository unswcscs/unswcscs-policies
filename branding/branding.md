# Logo

The CSCS logo consists of the soaring bird emblem and the CSCS wordmark.

# Fonts

For print media, CSCS uses the Helvetica Neue typeface family. For screen media, CSCS uses (depending on system availability) either the Helvetica or the Arial typeface family.

Style              | Font size | Typeface family    | Typeface weight
------------------ | --------- | ------------------ | ---------------
Wordmark           | 36 pt     | Lucida Grande      | Regular
Heading (print)    | 24 pt     | Helvetica Neue     | UltraLight
Heading (screen)   | 2 em      | Helvetica or Arial | Bold
Body Text (print)  | 12 pt     | Helvetica Neue     | Regular
Body Text (screen) | 1 em      | Helvetica or Arial | Regular

# Colour Palette

The primary CSCS colour is purple. The secondary CSCS colour is green. These colours were chosen because they provide sufficient contrast for people with colour blindness.

For print media, CSCS uses the PANTONE+ Solid Coated palette. For screen media, CSCS uses the plum and chameleon colour sets from the [Tango Color Palette](http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines#Color_Palette).

Colour             | Highlight     | Midtone        | Shadow
------------------ | ------------- | -------------- | --------------
Primary (print)    | PANTONE 522 C | PANTONE 7661 C | PANTONE 7447 C
Primary (screen)   | `#ad7fa8`     | `#75507b`      | `#5c3566`
Secondary (print)  | PANTONE 375 C | PANTONE 375 C  | PANTONE 369 C
Secondary (screen) | `#8ae234`     | `#73d216`      | `#4e9a06`
